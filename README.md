# rinit bringup of Debian 9.5

This repository contains unit files for bringing up a Debian 9.5 system via
`rinit`, instead of via the default `systemd`.
